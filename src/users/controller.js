const debug = require("debug")("app:module-users-controller");
const { UsersService } = require("./services");
const createError = require("http-errors");
const { Response } = require("../common/response");

module.exports.UsersController = {
  getUsers: async (req, res) => {
    try {
      let users = await UsersService.getAll();
      Response.success(res, 200, "Lista de users", users);
      // res.json(products);
    } catch (error) {
      debug(error);
      Response.error(res);
      // res.status(500).json({ message: "Internal server error" });
    }
  },
  getUser: async (req, res) => {
    try {
      const {
        params: { id },
      } = req;
      let user = await UsersService.getById(id);
      if (!user) {
        Response.error(res, new createError.NotFound());
      } else {
        Response.success(res, 200, "Ver usuariuo", user);
      }
      // res.json(product);
    } catch (error) {
      debug(error);
      Response.error(res);
      // res.status(500).json({ message: `Internal server error` });
    }
  },
  createUser: async (req, res) => {
    try {
      const { body } = req;

      if (!body || Object.keys(body).length === 0) {
        Response.error(res, new createError.BadRequest());
      } else {
        const insertedId = await UsersService.create(body);

        Response.success(res, 201, "Usuario creado", insertedId);
      }

      // res.json(insertedId);
    } catch (error) {
      debug(error);
      Response.error(res);
      // res.status(500).json({ message: "Internal server error" });
    }
  },
};
