const { MongoClient } = require("mongodb");
const debug = require("debug")("app:module-db");

const { Config } = require("../config");

var connection = null;

module.exports.Database = (collection) =>
  new Promise(async (res, rej) => {
    ///patron singleton para una sola conexion
    try {
      if (!connection) {
        const client = new MongoClient(Config.mongoUri);
        connection = await client.connect();
        debug("New connection established MongoDB");
      }
      debug("Reusing connection");
      const db = connection.db(Config.mongoDB);
      res(db.collection(collection));
    } catch (error) {
      rej(error);
    }
  });
